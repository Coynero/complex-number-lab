/**
 * Fractions Manipulation
 * Created by Tim Coyne 
 * November 6 2016
 */

public class Fraction {

    private int num;
    private int den;
    
    public Fraction()
    {
        this.num = 1;
        this.den = 1;
    }
    
    public Fraction(int num, int den)
    {
        this.num = num;
        this.den = den;
    }
    
    private int common(int a, int b)
    {
        int j = 1;
        int _a = Math.abs(a);
        int _b = Math.abs(b);
        for (int i = 2; i <= Math.max(_a, _b); i++)
        {
            if (_a % i == 0 && _b % i == 0)
            {
                j = i;
            }
        }
        return j;
    }
    
    public Fraction add(Fraction f)
    {   
        int lcm = this.den * (f.den / common(this.den, f.den));
        int num = (this.num * (lcm / this.den)) + (f.num * (lcm / f.den));
        int gcf = common(num, lcm);
        return new Fraction(num / gcf, lcm / gcf);
    }
    
    public Fraction subtract(Fraction f)
    {
        return add(f.multiply(new Fraction(-1, 1)));
    }
    
    public Fraction multiply(Fraction f)
    {
        int num = this.num * f.num;
        int den = this.den * f.den;
        int common = common(num, den);
        return new Fraction(num / common, den / common);
    }
    
    public Fraction divide(Fraction f)
    {
        if (this.den == 0 || f.den == 0)
        {
            return new Fraction(0, 0);
        }
        return multiply(new Fraction(f.den, f.num));
    }
    
    public int compareTo(Fraction f)
    {
        if (f.den == 0 || this.den == 0) return 0;
        
        double curr = this.num / this.den;
        double othr = f.num / f.den;
        
        if (curr > othr)
        {
            return 1;
        }
        else if (othr > curr)
        {
            return -1;
        }
        return 0;
    }
    
    public boolean isEqualTo(Fraction f)
    {
        if ((this.num == f.getNumerator() && this.den == f.getDenominator()) || (f.getNumerator() == 0 && this.num == 0)) return true;
        return false;
    }
    
    public int getNumerator()
    {
        return this.num;
    }
    
    public int getDenominator()
    {
        return this.den;
    }
    
    @Override
    public String toString()
    {
        if (this.num < 0 && this.den < 0)
        {
            if (this.den == -1)
            {
                return -this.num + "";
            }
            return -this.num + "/" + -this.den;
        }
        else if (this.den == 0)
        {
            return "ERROR: Cannot divide by zero!";
        }
        else if (this.num == 0)
        {
            return "0";
        }
        else if (this.den == 1)
        {
            return this.num + "";
        }
        else if (this.den == -1)
        {
            return -this.num + "";
        }
        else if (Math.abs(num) == Math.abs(den))
        {
            if (den < 0)
            {
                return -this.num + "";
            }
            return this.num + "";
        }
        return this.num + "/" + this.den;
    }
}
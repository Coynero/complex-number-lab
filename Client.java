import java.util.ArrayList;

public class Client {

	public static void main(String[] args)
	{
		Complex c = new Complex(new Fraction(-5, 1), new Fraction(6, 1));
		Complex d = new Complex(new Fraction(3, 1), new Fraction(-4, 1));
		System.out.println(c.divide(d).toString());
	}
}

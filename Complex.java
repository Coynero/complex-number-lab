public class Complex 
{
	Fraction realNumber;
	Fraction nonRealNumber;
	
	public Complex(Fraction constant, Fraction coefficient)
	{
		this.realNumber = constant;
		this.nonRealNumber = coefficient;
	}
	
	public Complex add(Complex c)
	{
		return new Complex(this.realNumber.add(c.realNumber), this.nonRealNumber.add(c.nonRealNumber));
	}
	
	public Complex subtract(Complex c)
	{
		return new Complex(this.realNumber.subtract(c.realNumber), this.nonRealNumber.subtract(c.nonRealNumber));
	}
	
	public Complex multiply(Complex c)
	{
		//(ac - bd) + (ad + bc)i  = (a + bi) / (c + di)
		Fraction real = this.realNumber.multiply(c.realNumber).subtract(this.nonRealNumber.multiply(c.nonRealNumber));
		Fraction imaginary = this.realNumber.multiply(c.nonRealNumber).add(this.nonRealNumber.multiply(c.realNumber));
		return new Complex(real, imaginary);
	}
	
	public Complex divide(Complex c)
	{
		Complex conjugate = new Complex(c.realNumber, c.nonRealNumber.multiply(new Fraction(-1, 1)));
		Complex numerator = this.multiply(conjugate);
		Complex denominator = c.multiply(conjugate);
		return new Complex(numerator.realNumber.divide(denominator.getConstant()), numerator.nonRealNumber.divide(denominator.getConstant()));
	}
	
	public Fraction getConstant()
	{
		return this.realNumber;
	}
	
	public Fraction getCoefficient()
	{
		return this.nonRealNumber;
	}
	
	@Override
	public String toString()
	{
	    System.out.println((double) nonRealNumber.getNumerator() / nonRealNumber.getDenominator());
		if (((double) nonRealNumber.getNumerator() / nonRealNumber.getDenominator()) < 0)
		{
			return realNumber.toString() + " - " + nonRealNumber.multiply(new Fraction(-1, 1)).toString() + "i";
		}
		return realNumber.toString() + " + " + nonRealNumber.toString() + "i";
	}
}
